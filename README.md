# BandcampOBS

Joue les morceaux au hasard d'une bibliothèque d'achats Bandcamp en version sans perte audio dans une source OBS.

## Pré-requis

- Le logiciel [OBS](https://obsproject.com/).
- Le plugin [obs-websocket](https://github.com/Palakis/obs-websocket).
- [Node.js](https://nodejs.org) en version 13 ou supérieure.

## Installation

- [Télécharger le projet](https://gitlab.com/Desmu/utipobs/-/archive/master/bandcampobs-master.zip).

### Récupérer sa collection

- Télécharger [Greasemonkey pour Firefox](https://addons.mozilla.org/fr/firefox/addon/greasemonkey/) ou [Tampermonkey pour Chrome](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo), des plugins pour le navigateur permettant d'exécuter ses propres scripts sur une page web.
- Depuis le plugin téléchargé, installer le script `bandcampobs.user.js`.
- Se rendre sur [Bandcamp](https://bandcamp.com) et se connecter à son compte Bandcamp.
- Une barre d'actions s'est normalement ajoutée sur la partie supérieure du site. Le bouton `Récupérer une collection complète` générera un fichier contenant tous les titres et albums achetés, tandis que passer un fichier de collection au bouton `Actualiser une collection existante` permet de mettre à jour un fichier précédemment généré avec les nouveaux achats réalisés depuis la génération précédente.
- Un fichier `configuration.json` doit être proposé au téléchargement. Déposer ce fichier dans le dossier de téléchargement, puis vérifier la configuration du bloc `obs`.

### Ajouter les sources à OBS

- Exécuter la commande `npm install --prod` dans le dossier du programme depuis le terminal pour installer les librairies utilisées.
- Exécuter le fichier `install.js` avec le programme `Node.js` (par défaut situé à l'emplacement `C:\Program Files\nodejs\node.exe` sur Windows, s'il n'est pas affiché dans la liste des programmes disponibles). Cette action aura pour effet de créer ou recréer les trois sources utilisées, dans une nouvelle scène si le nom de scène spécifié dans la configuration n'est pas trouvé dans OBS.
- Faire un clic droit sur la source `BandcampBrowser` affichant le site Bandcamp puis sélectionner `Interagir`, y entrer les identifiants du site Bandcamp duquel récupérer la configuration et se connecter.

## Jouer un son

- Exécuter la commande `node bandcampobs.js` dans le dossier du programme depuis le terminal pour jouer un son au hasard.
- Appuyer sur Entrée dans le terminal pour changer de piste en cours de lecture.

## Configuration

* `bandcamp` : paramètres liés au script utilisateur Bandcamp.
  - `fanId` : identifiant du compte Bandcamp concerné.
  - `count` : nombre d'articles récupérés (`1000` par défaut).
* `obs` : paramètres liés à OBS.
  - `wsAddress` : adresse utilisée par OBS Websocket (visible dans le menu d'OBS `Outils > Paramètres du serveur WebSockets`, `"localhost:4444"` par défaut).
  - `wsPassword` : mot de passe utilisé par OBS WebSocket (visible dans le menu d'OBS `Outils > Paramètres du serveur WebSockets`, laisser vide si l'authentification n'est pas activée).
  - `sceneName` : nom de la scène OBS dans laquelle sont ajoutées les sources OBS concernées (`"SceneName"` par défaut, si la scène n'existe pas, elle est créée).
  - `browserSourceName` : nom de la source navigateur créée pour afficher le navigateur Bandcamp et jouer le son obtenu (`"BandcampBrowser"`).
  - `imageSourceName` : nom de la source navigateur créée pour afficher la pochette de l'album sélectionné (`"BandcampImage"`).
  - `textSourceName` : nom de la source texte créée pour afficher les informations du titre sélectionné (`"BandcampText"`).
  - `textSourceText` : contenu de la source texte créée pour afficher les informations du titre sélectionné (`"$artist\n$title\n$album\n$url"`, les variables disponibles sont `$artist` pour le nom d'artiste, `$title` pour le nom du titre, `$album` pour le nom de l'album, `$url` pour le lien de l'album sur Bandcamp).
  - `secondsDelay` : nombre de secondes d'attente s'écoulant entre chaque titre, pour gérer le décalage entre son chargement et son lancement (`5`).
* `items` : tableau des albums récupérés depuis le script utilisateur, un album comprend les informations suivantes :
  - `item_id` : identifiant de l'album sur Bandcamp.
  - `band_name` : nom de l'artiste de l'album.
  - `item_title` : nom de l'album.
  - `item_url` : lien de l'album sur Bandcamp.
  - `item_art_url` : lien de la pochette de l'album.
  - `tracks` : liste des titres de l'album, un titre comprend les informations suivantes :
    * `id` : identifiant du titre sur Bandcamp.
    * `artist` : nom de l'artiste du titre.
    * `title` : nom du titre.
    * `duration` : durée du titre en secondes.
    * `file` : lien du titre au format MP3 VO.
    * `visible` : true pour permettre au titre d'être joué au hasard.

## Librairies utilisées

- [obs-websocket-js](https://github.com/haganbmj/obs-websocket-js) pour la liaison à OBSWebSocket.
