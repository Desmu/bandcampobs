import fs from 'fs';
import path from 'path';
import readline from 'readline';
import url from 'url';
import OBSWebSocket from 'obs-websocket-js';

const obs = new OBSWebSocket();
const configuration = JSON.parse(fs.readFileSync(path.resolve(path.dirname(url.fileURLToPath(import.meta.url)), './configuration.json'), 'utf8'));
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});
let timeout = null;

function newTrack() {
  let album = null;
  let track = null;
  let isTrackVisible = false;
  while (!isTrackVisible) {
    album = configuration.items[Math.floor(Math.random() * Math.floor(configuration.items.length))];
    track = album.tracks[Math.floor(Math.random() * Math.floor(album.tracks.length))];
    if (track.visible) {
      isTrackVisible = true;
    }
  }
  obs.send('SetSourceSettings', {
    sourceName: configuration.obs.textSourceName,
    sourceSettings: {
      text: configuration.obs.textSourceText.replace('$artist', track.artist).replace('$title', track.title).replace('$album', album.item_title).replace('$url', album.item_url),
    },
  }).catch((error) => { console.log(error); });
  obs.send('SetSourceSettings', {
    sourceName: configuration.obs.imageSourceName,
    sourceSettings: {
      url: album.item_art_url,
    },
  }).catch((error) => { console.log(error); });
  obs.send('SetSourceSettings', {
    sourceName: configuration.obs.browserSourceName,
    sourceSettings: {
      url: track.file,
      height: 0,
      width: 0,
    },
  }).then(() => {
    timeout = setTimeout(() => {
      newTrack();
    }, (track.duration + configuration.obs.secondsDelay) * 1000);
  }).catch((error) => { console.log(error); });
}

function newPrompt() {
  rl.question('', () => {
    clearTimeout(timeout);
    newTrack();
    newPrompt();
  });
}

rl.on('close', () => {
  obs.send('SetSourceSettings', {
    sourceName: configuration.obs.textSourceName,
    sourceSettings: {
      text: '',
    },
  }).catch((error) => { console.log(error); });
  obs.send('SetSourceSettings', {
    sourceName: configuration.obs.imageSourceName,
    sourceSettings: {
      url: '',
    },
  }).catch((error) => { console.log(error); });
  obs.send('SetSourceSettings', {
    sourceName: configuration.obs.browserSourceName,
    sourceSettings: {
      url: '',
    },
  }).catch((error) => { console.log(error); });
  process.exit(0);
});

obs.connect({ address: configuration.obswsAddress, password: configuration.obswsPassword }).then(() => {
  newTrack();
  newPrompt();
});
