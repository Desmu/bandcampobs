// ==UserScript==
// @name        Bandcamp Collection Dump
// @namespace   https://bandcamp.com/
// @description Récupère les données d'une collection Bandcamp pour une utilisation dans BandcampOBS.
// @include     https://bandcamp.com/*
// @version     1
// @grant       none
// @author      Desmu
// ==/UserScript==

let configuration = {
  bandcamp: {
    fanId: null,
    count: 1000,
  },
  obs: {
    wsAddress: 'localhost:4444',
    wsPassword: '',
    sceneName: 'SceneName',
    browserSourceName: 'BandcampBrowser',
    imageSourceName: 'BandcampImage',
    textSourceName: 'BandcampText',
    textSourceText: '$artist\n$title\n$album\n$url',
    secondsDelay: 5
  },
  items: [],
};

async function getBandcampCollectionFile() {
  document.querySelector('#bandcampobs_infos').innerHTML = 'Génération du fichier de collection en cours.';
  const data = await fetch('https://bandcamp.com/api/fancollection/1/collection_items', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: JSON.stringify({
      fan_id: configuration.bandcamp.fanId,
      older_than_token: '0:0:t:0:',
      count: configuration.bandcamp.count,
    }),
  }).then((res) => res.json());
  data.items.forEach((item) => {
    const tracks = [];
    Object.entries(data.tracklists).some(([itemId, tracklist]) => {
      if (itemId.indexOf(item.item_id) !== -1) {
        tracklist.forEach((track) => {
          tracks.push({
            id: track.id,
            artist: track.artist,
            title: track.title,
            duration: track.duration,
            file: track.file['mp3-v0'],
            visible: true,
          });
        });
        return true;
      }
      return false;
    });
    if (!configuration.items.find((element) => (element.item_id && (element.item_id === item.item_id)))) {
      configuration.items.push({
        item_id: item.item_id,
        band_name: item.band_name,
        item_title: item.item_title,
        item_url: item.item_url,
        item_art_url: item.item_art_url,
        tracks,
      });
    }
  });
  const dataStr = `data:text/json;charset=utf-8,${encodeURIComponent(JSON.stringify(configuration, null, 2))}`;
  const downloadAnchorNode = document.createElement('a');
  downloadAnchorNode.setAttribute('href', dataStr);
  downloadAnchorNode.setAttribute('download', 'configuration.json');
  document.body.appendChild(downloadAnchorNode);
  downloadAnchorNode.click();
  downloadAnchorNode.remove();
  document.querySelector('#bandcampobs_infos').innerHTML = 'Exécution de BandcampOBS terminée. Fichier généré.';
}

function setBandcampCollectionFile() {
  const file = document.querySelector('#collection_json_part').files[0];
  if (file) {
    document.querySelector('#bandcampobs_infos').innerHTML = 'Fichier détecté, lecture en cours.';
    const reader = new FileReader();
    reader.readAsText(file, 'UTF-8');
    reader.onload = (evt) => {
      const content = JSON.parse(evt.target.result);
      if (content.bandcamp) {
        configuration = content;
        getBandcampCollectionFile();
      } else {
        document.querySelector('#bandcamp_infos').innerHTML = 'Erreur : fichier non issu de BandcampOBS.';
      }
    };
    reader.onerror = () => {
      document.querySelector('#bandcamp_infos').innerHTML = 'Erreur de lecture du fichier.';
    };
  }
}

window.onload = () => {
  if (localStorage.fanId) {
    configuration.bandcamp.fanId = parseInt(localStorage.fanId, 10);
    document.querySelector('#menubar-wrapper').insertAdjacentHTML('beforebegin', `
      <div id="bandcampobs" style="align-items: center; background-color: #1da0c3; color: white; display: flex; font-size: 16px; justify-content: space-between; padding: 10px;">
        <strong><a href="https://gitlab.com/Desmu/bandcampobs" style="color: white;">BandcampOBS</a></strong>
        <div>
          <label for="collection_json_part">Actualiser une collection existante :</label>
          <input type="file" id="collection_json_part" name="collection_json_part" style="display: inline;">
        </div>
        <input type="button" id="collection_json_full" value="Télécharger la collection complète" style="color: black;">
        <div id="bandcampobs_infos"></div>
      </div>
    `);
    document.querySelector('#collection_json_part').addEventListener('change', () => { setBandcampCollectionFile(); });
    document.querySelector('#collection_json_full').addEventListener('click', () => { getBandcampCollectionFile(); });
  }
};
