import fs from 'fs';
import path from 'path';
import url from 'url';
import OBSWebSocket from 'obs-websocket-js';

const obs = new OBSWebSocket();
const configuration = JSON.parse(fs.readFileSync(path.resolve(path.dirname(url.fileURLToPath(import.meta.url)), './configuration.json'), 'utf8'));

async function createTextSource() {
  await obs.send('CreateSource', {
    sceneName: configuration.obs.sceneName,
    sourceName: configuration.obs.textSourceName,
    sourceKind: (process.platform === 'win32') ? 'text_gdiplus' : 'text_ft2_source',
    sourceSettings: {
      text: configuration.obs.textSourceText,
    },
  }).catch((error) => { console.log(error); });
}

async function createImageSource() {
  await obs.send('CreateSource', {
    sceneName: configuration.obs.sceneName,
    sourceName: configuration.obs.imageSourceName,
    sourceKind: 'browser_source',
    sourceSettings: {
      height: 210,
      width: 210,
      url: 'https://f4.bcbits.com/img/',
    },
  }).catch((error) => { console.log(error); });
}

async function createBrowserSource() {
  await obs.send('CreateSource', {
    sceneName: configuration.obs.sceneName,
    sourceName: configuration.obs.browserSourceName,
    sourceKind: 'browser_source',
    sourceSettings: {
      height: 600,
      width: 800,
      url: 'https://bandcamp.com/login',
      reroute_audio: true,
    },
  }).then(async () => {
    await obs.send('SetAudioMonitorType', {
      sourceName: configuration.obs.browserSourceName,
      monitorType: 'monitorAndOutput',
    }).catch((error) => { console.log(error); });
  }).catch((error) => { console.log(error); });
}

async function deleteTextSource(itemId) {
  await obs.send('DeleteSceneItem', {
    scene: configuration.obs.sceneName,
    item: {
      id: itemId,
      name: configuration.obs.textSourceName,
    },
  }).catch((error) => { console.log(error); });
}

async function deleteImageSource(itemId) {
  await obs.send('DeleteSceneItem', {
    scene: configuration.obs.sceneName,
    item: {
      id: itemId,
      name: configuration.obs.imageSourceName,
    },
  }).catch((error) => { console.log(error); });
}

async function deleteBrowserSource(itemId) {
  await obs.send('DeleteSceneItem', {
    scene: configuration.obs.sceneName,
    item: {
      id: itemId,
      name: configuration.obs.browserSourceName,
    },
  }).catch((error) => { console.log(error); });
}

obs.connect({ address: configuration.obswsAddress, password: configuration.obswsPassword }).then(() => {
  obs.send('GetSceneList').then(async (responseGSL) => {
    let promises = [];
    if (!responseGSL.scenes.some((scene) => (scene.name === configuration.obs.sceneName))) {
      obs.send('CreateScene', { sceneName: configuration.obs.sceneName }).then(async () => {
        promises.push(createTextSource());
        promises.push(createImageSource());
        promises.push(createBrowserSource());
        await Promise.all(promises).then(async () => {
          obs.disconnect();
        });
      }).catch((error) => { console.log(error); });
    } else {
      obs.send('GetSceneItemList', { sceneName: configuration.obs.sceneName }).then(async (responseGSIL) => {
        responseGSIL.sceneItems.forEach((sceneItem) => {
          if (sceneItem.sourceName === configuration.obs.textSourceName) {
            promises.push(deleteTextSource(sceneItem.itemId));
          } else if (sceneItem.sourceName === configuration.obs.imageSourceName) {
            promises.push(deleteImageSource(sceneItem.itemId));
          } else if (sceneItem.sourceName === configuration.obs.browserSourceName) {
            promises.push(deleteBrowserSource(sceneItem.itemId));
          }
        });
        promises = [];
        await Promise.all(promises).then(async () => {
          promises.push(createTextSource());
          promises.push(createImageSource());
          promises.push(createBrowserSource());
          await Promise.all(promises).then(async () => {
            obs.disconnect();
          });
        });
      }).catch((error) => { console.log(error); });
    }
  }).catch((error) => { console.log(error); });
});
